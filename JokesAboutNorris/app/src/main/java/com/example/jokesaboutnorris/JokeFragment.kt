package com.example.jokesaboutnorris

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.jokesaboutnorris.databinding.FragmentJokeBinding
import com.example.jokesaboutnorris.recViewRes.ItemAdapter
import com.example.jokesaboutnorris.recViewRes.Joke
import com.example.jokesaboutnorris.recViewRes.Value
import com.example.jokesaboutnorris.retrofitDir.APIInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class JokeFragment : Fragment() {
    var count:String = ""
    lateinit var binding: FragmentJokeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentJokeBinding.inflate(inflater)
        setRetainInstance(true)
        val linearLayoutManager = LinearLayoutManager(binding.root.context)
        binding.recView.layoutManager = linearLayoutManager

        binding.button.setOnClickListener {
            if(binding.editTextNumber.text.toString()!=""){
                count = binding.editTextNumber.text.toString()
                getJokesList()
            }
            else{
                Toast.makeText(requireContext(), "Enter the count of jokes!", Toast.LENGTH_SHORT).show()
            }
        }

        return binding.root
    }


    fun getJokesList(){
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://api.icndb.com/jokes/random/$count/")
            .build()
            .create(APIInterface::class.java)

        val retrofitData = retrofitBuilder.getData()

        retrofitData.enqueue(object : Callback<Joke>{
            override fun onResponse(
                call: Call<Joke?>?,
                response: Response<Joke?>?
            ) {
                val responseBody = response?.body()!!

                val adapter = ItemAdapter(responseBody)
                adapter.notifyDataSetChanged()
                binding.recView.adapter = adapter



            }

            override fun onFailure(call: Call<Joke?>?, t: Throwable?) {

            }
        })
    }

    companion object {

        @JvmStatic
        fun newInstance() = JokeFragment()
    }
}