package com.example.jokesaboutnorris.recViewRes

data class Joke(
    val type: String,
    val value: List<Value>
)