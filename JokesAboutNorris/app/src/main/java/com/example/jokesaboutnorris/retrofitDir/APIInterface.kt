package com.example.jokesaboutnorris.retrofitDir

import com.example.jokesaboutnorris.recViewRes.Joke
import com.example.jokesaboutnorris.recViewRes.Value
import retrofit2.Call

import retrofit2.http.GET


interface APIInterface {

    @GET("posts")
    fun getData():Call<Joke>
}