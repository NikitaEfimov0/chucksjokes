package com.example.jokesaboutnorris.recViewRes

data class Value(
    val id: Int,
    val joke: String
)