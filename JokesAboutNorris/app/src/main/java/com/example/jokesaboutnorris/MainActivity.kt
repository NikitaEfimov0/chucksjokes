package com.example.jokesaboutnorris

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.navigation.findNavController
import com.example.jokesaboutnorris.databinding.ActivityMainBinding
import java.util.zip.Inflater

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(this.layoutInflater)
        setContentView(binding.root)
        var currentItem:Int = R.id.jokeMenuItem
        supportActionBar?.setTitle("Jokes")
        binding.bottomNavMenu.setOnItemSelectedListener {
            when(it.itemId){
                R.id.jokeMenuItem ->{
                    if(it.itemId == currentItem){
                        findNavController(R.id.fragmContainer).navigate(R.id.jokeSelf)
                    }
                    else{
                        findNavController(R.id.fragmContainer).navigate(R.id.toJoke)
                        supportActionBar?.setTitle("Jokes")
                        currentItem = it.itemId
                    }
                }
                R.id.webMenuItem ->{
                    if(it.itemId == currentItem){
                        findNavController(R.id.fragmContainer).navigate(R.id.apiSelf)
                    }
                    else{
                        findNavController(R.id.fragmContainer).navigate(R.id.toApi)
                        supportActionBar?.setTitle("Api info")
                        currentItem = it.itemId
                    }
                }

            }
            true
        }


    }
}