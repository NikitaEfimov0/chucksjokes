package com.example.jokesaboutnorris

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.jokesaboutnorris.databinding.FragmentApiBinding

class ApiFragment : Fragment() {
    lateinit var binding: FragmentApiBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentApiBinding.inflate(inflater)
        setRetainInstance(true)
        binding.web.loadUrl("http://www.icndb.com/api/")

        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance() = ApiFragment()
    }
}